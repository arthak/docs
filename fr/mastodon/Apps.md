Liste d'applications
====================

Il existe de nombreuses applications compatibles avec Mastodon :

*Cette liste est tirée de la page de [documentation officielle](https://github.com/tootsuite/documentation/blob/master/Using-Mastodon/Apps.md), qui est susceptible d'être davantage à jour.*

## Clients mobiles

### Android

|Application|Code source|Développeu·r·se·s|
|---|-----------|------------|
|[11t](https://play.google.com/store/apps/details?id=com.jeroensmeets.mastodon)|<https://github.com/jeroensmeets/mastodon-app>|[@jeroensmeets@mastodon.social](https://mastodon.social/users/jeroensmeets)|
|[AndStatus](http://andstatus.org/)|<https://github.com/andstatus/andstatus>|[@AndStatus@mastodon.social](https://mastodon.social/@AndStatus)|
|[Subway Tooter](https://play.google.com/store/apps/details?id=jp.juggler.subwaytooter)|<https://github.com/tateisu/SubwayTooter>|[@tateisu@mastodon.juggler.jp](https://mastodon.juggler.jp/@tateisu)|
|[Tusky](https://f-droid.org/repository/browse/?fdcategory=Internet&fdid=com.keylesspalace.tusky&fdpage=3&fdstyle=grid)|<https://github.com/Vavassor/Tusky>|[@Vavassor@mastodon.social](https://mastodon.social/users/Vavassor)|
|[Twidere](https://github.com/TwidereProject/Twidere-Android#twidere-for-android)|<https://github.com/TwidereProject/Twidere-Android>|[@mariotaku@pawoo.net](https://pawoo.net/@mariotaku)|
|[Mastobone](https://play.google.com/store/apps/details?id=com.mastobone) (alpha)|<https://github.com/deguchi/mastobone>|[@deguchi@bookn.me](https://bookn.me/@deguchi)|
|[MouseApp](https://f-droid.org/repository/browse/?fdfilter=mastodon&fdid=fr.xtof54.mousetodon) (beta)|<https://github.com/cerisara/mousetodon>|[@cerisara@mastodon.etalab.gouv.fr](https://mastodon.etalab.gouv.fr/@cerisara)
|[TootyFruity](https://play.google.com/store/apps/details?id=ch.kevinegli.tootyfruity221258) (beta)|<https://github.com/eggplantPrince/tootyFruity>|[@eggplant@mastodon.social](https://mastodon.social/users/eggplant)|

Nous recommandons Twidere et Tusky (toutefois cette dernière semble ne pas fonctionner en version 1.1.4)

### iOS

|Application|Code source|Développeu·r·se·s|
|---|-----------|------------|
|[11t](https://appsto.re/i67Q3LH)|<https://github.com/jeroensmeets/mastodon-app>|[@jeroensmeets@mastodon.social](https://mastodon.social/users/jeroensmeets)|
|[Amaroq](https://itunes.apple.com/us/app/amarok-for-mastodon/id1214116200?ls=1&mt=8)|<https://github.com/ReticentJohn/Amaroq>|[@eurasierboy@mastodon.social](https://mastodon.social/users/eurasierboy)|
|[TootyFruity](https://docs.google.com/forms/d/e/1FAIpQLScW2lvZaKrOm4AKJH5HLI_Ul0Yr3CsXulf5bcTJQ1xaN5jiPg/viewform?c=0&w=1) (beta)|<https://github.com/eggplantPrince/tootyFruity>|[@eggplant@mastodon.social](https://mastodon.social/users/eggplant)|

## Applications web alternatives
|Application|Code source|Développeu·r·se·s|
|---|-----------|------------|
|[Halcyon](https://halcyon.social)|<https://github.com/halcyon-suite/halcyon-for-mastodon>|[@neet@mastodon.social](https://mastodon.social/@neet)|
|[Naumanni](https://naumanni.com/) (alpha)|<https://github.com/naumanni/naumanni>|[@shi3z@mstdn.onosendai.jp](https://mstdn.onosendai.jp/@shi3z)/[@shn@oppai.tokyo](https://oppai.tokyo/@shn)|

## Applications de bureau
### Linux

|Application|Code source|Développeu·r·se·s|
|---|-----------|------------|
|[Mstdn](https://github.com/rhysd/Mstdn) | <https://github.com/rhysd/Mstdn>|[@Linda_pp@mstdn.jp](https://mstdn.jp/@Linda_pp)/[@inudog@mastodon.social](https://mastodon.social/@inudog) |

### Mac ###

|Application|Code source|Développeu·r·se·s|
|---|-----------|------------|
|[Mstdn](https://github.com/rhysd/Mstdn) | <https://github.com/rhysd/Mstdn>|[@Linda_pp@mstdn.jp](https://mstdn.jp/@Linda_pp)/[@inudog@mastodon.social](https://mastodon.social/@inudog) |

### Windows

|Application|Version|Code source|Développeu·r·se·s|
|---|-------|-----------|------------|
|[Capella](https://coolstar.org/capella) | 7, 8.1, 10 | <https://github.com/coolstar/Capella> | [@coolstar@mastodon.social](https://mastodon.social/users/coolstar)|
|[Flantter](https://www.microsoft.com/store/apps/9wzdncrcrpmn)|10|<https://github.com/cucmberium/Flantter.MilkyWay>|[@cucmberium@mstdn.maud.io](https://mstdn.maud.io/@cucmberium)|
|[Mstdn](https://github.com/rhysd/Mstdn) | 8.1 (confirmé) |<https://github.com/rhysd/Mstdn>|[@Linda_pp@mstdn.jp](https://mstdn.jp/@Linda_pp)/[@inudog@mastodon.social](https://mastodon.social/@inudog) |
|[WinMasto](https://github.com/drasticactions/WinMasto)|10|<https://github.com/drasticactions/WinMasto>|[@drasticactions@mastodon.network](https://mastodon.network/users/drasticactions)|
|[Tuuto](https://www.microsoft.com/store/apps/9nh0493n4tsb)|10|<https://github.com/Tlaster/Tuuto>|[@Tlaster@mstdn.jp](https://mstdn.jp/@Tlaster)|
|[Mastodon UWP](https://github.com/woachk/mastodon/releases) (beta)|10|<https://github.com/woachk/mastodon>|[@my123@mastodon.social](https://mastodon.social/users/my123)|

## Autres applications

|Application|Plateforme|Code source|Développeu·r·se·s|
|---|--------|-----------|------------|
|[madonctl](https://github.com/McKael/madonctl)|CLI|<https://github.com/McKael/madonctl>|[@McKael@mamot.fr](https://mamot.fr/@McKael)|
|[mastodon.el](https://github.com/jdenen/mastodon.el)|Emacs|<https://github.com/jdenen/mastodon.el>|[@johnson@mastodon.social](https://mastodon.social/users/johnson)|
|[Mstdn (Mastodon CLI)](https://github.com/mattn/go-mastodon)|CLI|<https://github.com/mattn/go-mastodon>|[@mattn@mstdn.jp](https://mstdn.jp/@mattn)|
|[toot](https://github.com/ihabunek/toot)|CLI|<https://github.com/ihabunek/toot>|[@ihabunek@mastodon.social](https://mastodon.social/users/ihabunek)|
|[tootstream](https://github.com/magicalraccoon/tootstream)|CLI|<https://github.com/magicalraccoon/tootstream>|[@Raccoon@mastodon.social](https://mastodon.social/users/Raccoon)|
|[soCLIal](https://gitlab.com/IvanSanchez/soclial)|CLI|<https://gitlab.com/IvanSanchez/soclial>|[@IvanSanchez@mastodon.social](https://mastodon.social/@IvanSanchez)|

## Extensions de navigateur / Modules complémentaires

|Application|Navigateur|Code source|Développeu·r·se·s|
|---|--------|----|------------|
|Mastodon Lang Remover|[Chrome(ium)/Firefox](https://github.com/arthurlacoste/mastodon-lang-remover#install)|<https://github.com/arthurlacoste/mastodon-lang-remover>|[@arthak@mastodon.social](https://mastodon.social/users/arthak)|
|Tooter|[Chrome(ium)](https://chrome.google.com/webstore/detail/tooter/okmlpjijminjkikninbkcnfmhkofgnnk)/[Firefox](https://addons.mozilla.org/nl/firefox/addon/tooter/)|<https://github.com/ineffyble/tooter>|[@effy@mastodon.social](https://mastodon.social/users/effy)|
|Mastodon Share|[Firefox](https://addons.mozilla.org/nl/firefox/addon/mastodon-share/)|<https://github.com/kernox/Mastoshare>|[@Hellexis@framapiaf.org](https://framapiaf.org/@Hellexis)
|Fastoot|[Chrome(ium)](https://chrome.google.com/webstore/detail/fastoot/hnmnnhfeigiogjagmmpnhelpnhnchaoj)|<https://github.com/noraworld/fastoot>|[@noraworld@mastodon.noraworld.jp](https://mastodon.noraworld.jp/users/noraworld)|

## Ponts vers et depuis d'autres plateformes

|Application|Plateforme|Code source|Développeu·r·se·s|
|---|--------|-----------|------------|
|MastodonToTwitter|CLI|<https://github.com/halcy/MastodonToTwitter>|[@halcy@mastodon.social](https://mastodon.social/@halcy)|
|Retoot|CLI|<https://gitlab.com/mojo42/retoot>|[@Mojo@apoil.org](https://apoil.org/users/mojo)|
|RSSTootalizer|Navigateur web|<https://github.com/blind-coder/rsstootalizer>|[@blindcoder@toot.berlin](https://toot.berlin/users/blindcoder)|
|t2m - Twitter 2 Mastodon|CLI|<https://github.com/Psycojoker/t2m>|Psycojoker|
|umrc|IRC|<https://github.com/Ulrar/umrc>|[@lemonnierk@ulrar.net](https://mastodon.ulrar.net/users/lemonnierk)|
|[WordPress](http://mastodon.tools/wordpress/)|Plugin WordPress |<https://github.com/DavidLibeau/mastodon-tools/tree/master/wordpress>|[@David@mastodon.xyz](https://mastodon.xyz/@David)|
|[ogp-share](http://mastodon.tools/ogp-share/) (beta)|Navigateur web |<https://github.com/DavidLibeau/mastodon-tools/tree/master/ogp-share>|[@David@mastodon.xyz](https://mastodon.xyz/@David)|

## Bots

|Application|Plateforme|Code source|Développeu·r·se·s|
|---|--------|-----------|------------|
|HackerNewsBot|CLI|<https://github.com/raymestalez/mastodon-hnbot>|[@rayalez@hackertribe.io](https://hackertribe.io/users/rayalez)|
|mastodon-glossary-bot|CLI ou Heroku |<https://github.com/michelbl/mastodon-glossary-bot>|[@Michel@mastodon.etalab.gouv.fr](https://mastodon.etalab.gouv.fr/@Michel)|

## Autres outils
|Application|Plateforme|Code source|Développeu·r·se·s|
|---|--------|-----------|------------|
|[Contest-draw](http://mastodon.tools/contest-draw/)|Navigateur web |<https://github.com/DavidLibeau/mastodon-tools/tree/master/contest-draw>|[@David@mastodon.xyz](https://mastodon.xyz/@David)|
|[wall](http://mastodon.tools/wall/)|Navigateur web |<https://github.com/DavidLibeau/mastodon-tools/tree/master/wall>|[@David@mastodon.xyz](https://mastodon.xyz/@David)|
|[Scheduler](http://mastodon.tools/scheduler/) (beta)|Navigateur web |<https://github.com/DavidLibeau/mastodon-scheduler>|[@David@mastodon.xyz](https://mastodon.xyz/@David)|
