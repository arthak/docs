# Framagit

Framagit est la forge logicielle de Framasoft reposant sur le logiciel [Gitlab](https://gitlab.com).

Elle est ouverte à tous, dans la limite de 42 projets par personne.
Les projets peuvent être publics ou privés.

Avec Gitlab, nous proposons aussi de l'intégration continue avec [GitlabCI](https://docs.gitlab.com/ce/ci/README.html) et l'hébergement de pages statiques avec [Gitlab Pages](https://docs.gitlab.com/ce/user/project/pages/index.html) (voir notre [documentation](gitlab-pages.md))


## Débuter avec GitLab

Nous vous proposons aussi un tutoriel pour les débutants pour apprendre les bases de l'utilsation de Framagit/GitLab.


  - [1 - Quelques mots d'introduction sur Git](1-introduction.html)
  - [2 - La création et la configuration de votre compte sur Framagit](2-creation-configuration-compte.html)
  - [3 - Installation de votre poste de travail](3-installation-poste.html)
  - [4 - Comment gérer la communication au sein des membres de l'équipe](4-gerer-communication.html)
  - [5 - Activités sur le projet](5-activites-projet.html)

