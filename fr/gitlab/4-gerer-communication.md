# Gérer la communication : Issues / Commentaires

## Ce que nous allons apprendre

*  Comment communiquer au sein d'un projet à partir des _Issues_
*  Interpeller un membre du projet


## Procédons par étapes

Comme vous l'avez compris depuis le début de ce tutoriel, Git parle en anglais, et le terme « _Issue_ » dans cette langue et dans ce contexte peut signifier question ou problème. Donc sur le site Framagit et dans nos projets nous avons des rubriques « Issues » qui sont des fils de messages à propos d'une question.

![](images/Issue-1.png)

Pour aller dans une « _Issue_ », il faut dans Framagit naviguer dans Projets &gt; Choisir son projet (ici « Emmabuntus/Communication ») &gt; Issues

On sélectionne ici « Utilisation de Framagit »

![](images/Issue-2.png)

Pour laisser un nouveau commentaire, on clique sur « **Write** »

![](images/Issue-3.png)

Une fonctionnalité intéressante de cet outil est l'utilisation du signe « @ » qui ouvre une liste des pseudos des membres du collectif, ainsi que « All » (tout le monde).

Ici nous allons choisir « All » car nous voulons avertir tout le monde, mais on pourrait aussi choisir des pseudos individuels.

![](images/Issue-4.png)

On termine l'édition du message puis on appuie sur la touche verte « **Comment** ».

![](images/Issue-4.png)

La page de cette « _Issue_ » montre bien le message qui vient d'être créé, et de plus les membres adressés par le « @ » (donc, dans cet exemple, tout le monde, sauf vous-même) vont recevoir un eMail contenant le message ;

Par exemple lorsque _Launchy_ a écrit le premier commentaire j'ai reçu le message suivant :

![](images/Email-2.png)

Souvenez-vous que comme expliqué dans le chapitre [3.4](3-installation-poste), il vaut mieux prévenir le collectif quand on va modifier un fichier de type .odt. C'est précisément avec ce mécanisme des « Issues » que cela doit être fait, avant les modifications, puis après le « _push_ » de la modification sur le serveur.


## Faisons le point

Maintenant que vous savez envoyer votre premier document, nous allons voir les [Activités sur le projet](5-activites-projet.html).
