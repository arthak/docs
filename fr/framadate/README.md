# Framadate

Framadate est un service en ligne permettant de planifier un rendez-vous
ou prendre des décisions rapidement et simplement.
Aucune inscription préalable n’est nécessaire.

Voici comment ça fonctionne :

 1. Créez un sondage
 -  Déterminez les dates ou les sujets à choisir
 -  Envoyez le lien du sondage à vos amis ou collègues
 -  Discutez et prenez votre décision

Tutoriel pour bien débuter&nbsp;: [Planifier un rendez-vous rapidement
avec Framadate](prise-en-main.html)

### Pour aller plus loin&nbsp;:

-   Utiliser [Framadate](https://framadate.org)
-   [Installer
    Framadate](https://framacloud.org/fr/cultiver-son-jardin/framadate.html)
    sur votre serveur (tuto)
-   Un service proposé dans le cadre de la campagne [Dégooglisons
    Internet](https://degooglisons-internet.org)
