# Images

Il est possible d'ajouter des images à l'intérieur des présentations depuis deux manières : en téléversant les images ou bien en insérant directement l'adresse de l'image.

Note : Lors de l'insertion des images, celles-ci sont insérées dans leur taille originelle et peuvent s'afficher très grandes (plus grandes que la diapositive). Il faut alors les redimensionner à la main.

## Téléverser les images présentes sur son appareil

Cliquez sur le bouton « Image » en haut de l'interface puis cliquez sur le bouton « Téléverser » dans l'interface qui s'est ouverte.

Cela ouvre une boîte de dialogue vous permettant de choisir vos fichiers. Une fois le fichier choisi, le fichier va être téléversé sur les serveurs de Framaslides.

Une fois que cela est fait, l'image est visible dans la zone d'aperçu en dessous. Vous pouvez alors cliquer sur le bouton en bas à droite « Insérer une image ».

## Intégrer des images par leur URL

Si vous avez l'URL d'une image à intégrer, vous pouvez cliquer sur le bouton « Image » en haut de l'interface puis coller votre URL dans le champ de texte. L'image correspondante devrait alors s'afficher dans la zone d'aperçu en dessous et vous pouvez alors cliquer sur le bouton en bas à droite « Insérer une image ».
